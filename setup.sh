sudo mkdir -p ~/ariba/hana
sudo chmod 757 ~/ariba/hana
docker run -p 39013:39013 -p 39017:39017 -p 39041-39045:39041-39045 -p 1128-1129:1128-1129 -p 59013-59014:59013-59014 \
-v /Users/i836085/ariba/hana:/hana/ariba:rw \
--ulimit nofile=1048576:1048576 \
--sysctl kernel.shmmax=1073741824 \
--sysctl net.ipv4.ip_local_port_range='40000 60999' \
--sysctl kernel.shmmni=524288 \
--sysctl kernel.shmall=8388608 \
--name hana \
store/saplabs/hanaexpress:2.00.045.00.20200121.1 \
--passwords-url https://bitbucket.org/arasanr/hanasetup/raw/398be112831caea17cec432d3a93bca8c91228ee/master_password.json \
--agree-to-sap-license